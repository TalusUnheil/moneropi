#! /usr/bin/env bash

# Author: Talus Unheil
# License: AGPLv3+ (https://www.gnu.org/licenses/agpl-3.0.en.html)
# Source: https://gitlab.com/TalusUnheil/moneropi/-/blob/master/Bash/monero_blockchain_sync_bot.sh

clear

if [[ -z $1 ]]
    then
        read -p "Please enter the interval of the status check (in minutes, default: 10): " status_check_interval
        status_check_interval=${status_check_interval:-10}
    else
        status_check_interval=$1
fi

while true; do
    monero_blockchain_sync_percentage=$(monerod --rpc-ssl=disabled status | grep Height | cut --delimiter "(" --field 2 | cut --delimiter ")" --fields 1)
    if [[ $monero_blockchain_sync_percentage == "100.0%" ]]
        then
            # uncomment the 'sendxmpp'-pipe for push-notifications and/or uncomment the 'exit'-pipe to stop when fully synced
            echo "$(date +%R) The Monero Blockchain is fully synced!" # | sendxmpp --tls-ca-path /etc/ssl/certs/ --tls user@foo.bar # && exit
        else
            echo "$(date +%R) The Monero Blockchain is still syncing...(${monero_blockchain_sync_percentage})" # | sendxmpp --tls-ca-path /etc/ssl/certs/ --tls user@foo.bar
    fi
    sleep $(( status_check_interval * 60 ))
done
