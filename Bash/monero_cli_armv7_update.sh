#! /usr/bin/env bash

clear

# Author: Talus Unheil
# License: AGPLv3+

echo "# Updating the Monero CLI Daemon"
echo

# Check if monerod is already running and stop the daemon if so
echo "## Check if monerod is already running and stop the daemon if so..."
if [[ -n $(pgrep monerod) ]]; then
    monerod exit
fi
echo

# Download latest binaries and hashes
echo -n "## Downloading latest binary (archive) and hashes..."
wget --quiet --output-document /tmp/monero-linux-armv7.tar.bz2 https://downloads.getmonero.org/cli/linuxarm7
wget --quiet --output-document /tmp/monero-hashes.txt https://getmonero.org/downloads/hashes.txt
echo "Done!"
echo

# Verifying the hash file
echo "## Verifying the hash file:"
gpg --verify /tmp/monero-hashes.txt
echo
read -p "Hit enter if you see 'Good signature from \"binaryFate <binaryfate@getmonero.org>\"' or cancel with Ctrl+C: "

clear

# Verify the fingerprint of the archive
echo "## Please verify the calculated fingerprint of the downloaded archive..."
sha256sum /tmp/monero-linux-armv7.tar.bz2
echo
echo "...with the signed fingerprint of the hash file:"
cat /tmp/monero-hashes.txt | grep monero-linux-armv7
echo
read -p "Hit enter to continue (if fingerprints do match) or cancel with Ctrl+C (if fingerprints do not match): "

clear

sudo monero_cli_armv7_binary_handling.sh
