#! /usr/bin/env bash

clear

# Removing old versions as superuser
echo -n "## Removing old versions as superuser..."
rm /usr/local/bin/monero*
rm -rf /opt/monero*
echo "Done!"

# Unpacking the new binary files from the archive
echo -n "## Unpacking the new binary files from the archive..."
tar --extract --bzip2 --file /tmp/monero-linux-armv7.tar.bz2 --directory /opt/
echo "Done!"

# Removing the downloaded archive and hash files...
echo -n "## Removing the downloaded archive and hash files..."
rm /tmp/monero-linux-armv7.tar.bz2 /tmp/monero-hashes.txt
echo "Done!"

# Linking binaries from '/opt/monero*/monero*' to '/usr/local/bin/'...
echo -n "## Linking binaries from '/opt/monero*/monero*' to '/usr/local/bin/'..."
for file in /opt/monero*/monero*; do
    ln -s $file /usr/local/bin/$(basename $file)
done
echo "Done!"

# Starting the new Monero Daemon...
echo "# ALL DONE! Start the daemon as normal user with he command 'monerod --detach'..."
exit
