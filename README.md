[![](https://gitlab.com/TalusUnheil/moneropi/-/raw/master/media/MoneroPi_Logo_height_200_px.png)](https://gitlab.com/TalusUnheil/moneropi/-/raw/master/media/MoneroPi_Logo_height_200_px.png)

# MoneroPi

The aim of the MoneroPi project is to operate a Monero CLI Full Node on a (headless) Raspberry Pi.

*  Die deutsche Dokumentation ist zur Zeit in [DokuWiki-](https://gitlab.com/TalusUnheil/moneropi/-/blob/master/DokuWiki_DE.txt) und [Markdown-](https://gitlab.com/TalusUnheil/moneropi/-/blob/master/Markdown_DE.md)Syntax erfügbar. Eine Portierung auf HTML-Syntax ist geplant.
*  The english documentation is currently available in [DokuWiki-](https://gitlab.com/TalusUnheil/moneropi/-/blob/master/DokuWiki_EN.txt) und [Markdown-](https://gitlab.com/TalusUnheil/moneropi/-/blob/master/Markdown_EN.md)Syntax. A port to HTML-Syntax is planned.

Copyright (C)  2020  TALUS UNHEIL.
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".