[![](https://gitlab.com/TalusUnheil/moneropi/-/raw/master/media/MoneroPi_Logo_height_200_px.png)](https://gitlab.com/TalusUnheil/moneropi/-/raw/master/media/MoneroPi_Logo_height_200_px.png)

MoneroPi
========

Das Ziel des MoneroPi-Projekts ist der Betrieb eines Monero CLI Full Node auf einem (\'headless\') Raspberry Pi.

Vorwort
-------

Diese Dokumentation entstand im Rahmen des deutschsprachigen Podcasts \"MoneroMumble\" ([MoneroMumble.de](https://moneromumble.de/)). Diese Dokumentation wurde (Stand Mai 2020) noch **nicht** getestet, sondern entstand parallel zu einem ersten Versuch. Diese Dokumentation ist daher noch in einer experimentellen Phase und sollte nicht im Zusammenhang mit echten Werten (XMR) verwendet werden!

Der aktuelle Stand der Dokumentation ist [auf GitLab verfügbar](https://gitlab.com/TalusUnheil/moneropi) und unter [GNU FDLv1.3](https://www.gnu.org/licenses/fdl-1.3.html) lizenziert.

Wenn dir diese Dokumentation gefällt, oder sie hilfreich für dich war, dann würde ich mich über eine Spende freuen!

|                                          Monero Spende                                            |
| :-----------------------------------------------------------------------------------------------: |
| ![](https://gitlab.com/TalusUnheil/moneropi/-/raw/master/media/MoneroPi-XMR-Donation-QR-Code.png) |
|  86KZCj342BGBsdeTR6thhiagLJqrM4mhd1PEoj5e8koiNdXW4yYnFnmE73sDstAZjcWmgAyK37gWkNHgoJ7fivUqShwStrS  |

Hardware
--------

-   [Raspberry Pi 3 Model B](https://geizhals.de/raspberry-pi-3-modell-b-a1400349.html)
-   [Netzteil](https://geizhals.eu/raspberry-pi-3-b-official-power-supply-a1423317.html)
-   [Netzwerkkabel](https://geizhals.de/?cat=kabelnw&xf=8234_Kupfer)
-   [min. 4 GB MicroSD-Karte](https://geizhals.eu/?cat=sm_sdhc&xf=15024_microSD) (im Versuch lieferte `df --human` weniger als 3 GB tatsächlich genutzten Speicher)
-   [USB zu SATA Adapter Kabel](https://geizhals.de/?cat=hdadko&xf=11184_SATA%7E11189_USB%7E11190_Kabel%2FDongle)
-   [2,5\"-SSD](https://geizhals.eu/?cat=hdssd&xf=4836_2) (auf der Seite <https://moneroblocks.info/stats/blockchain-growth> findet man ganz unten den aktuellen Speicherbedarf der Blockchain - bei der Beschaffung der SSD bitte ausreichend Reserve einplanen!)

MicroSD-Karte vorbereiten
-------------------------

Diese Anleitung setzt einen Linux-PC voraus. Konkret wurde diese Anleitung an einem PC mit Linux Mint 19 Cinnamon erstellt.

1.  [Raspbian Lite](https://downloads.raspberrypi.org/raspbian_lite_latest.torrent) herunterladen
2.  Prüfsumme berechnen und mit der Prüfsumme auf der Webseite abgleichen (CLI: `sha256sum ~/Downloads/*raspbian-buster-lite.zip`)
3.  Archiv entpacken (CLI: `unzip ~/Downloads/*raspbian-buster-lite.zip ~/Downloads/`)
4.  MicroSD-Karte mit dem PC verbinden und das Programm \"Laufwerke\" starten (GUI: Startmenü -\> Zubehör -\> Laufwerke; CLI: `gnome-disks`)
    1.  Laufwerk formatieren
    2.  Neue Partition erstellen (FAT)
    3.  Laufwerksabbild wiederherstellen -\> \~/Downloads/\*raspbian-buster-lite.img
5.  Download und Laufwerksabbild löschen (CLI: `rm ~/Downloads/*raspbian-buster-lite.zip ~/Downloads/*raspbian-buster-lite.img`)

### optional: SSH-Server aktivieren

Wenn man den MoneroPi nicht mit Bildschirm und Tastatur betreiben möchte (\'headless\'), dann empfiehlt sich die Steuerung per SSH. Der SSH-Server ist bei Raspbian allerdings standardmäßig nicht aktiviert, daher ist die manuelle Aktivierung erforderlich.

1.  Dateimanager öffnen
2.  unter \"Geräte\" die Partition \"boot\" der MicroSD-Karte öffnen
3.  eine leere Datei mit dem Namen \"ssh\" ohne Dateiendung erstellen (CLI: `touch /media/$USER/boot/ssh`)

### optional: SSH-Schlüssel übertragen

Zur Steuerung des MoneroPi via SSH sollte zur Sicherheit ein SSH-Schlüssel statt eines Passworts zur Anmeldung werden. In diesem Beispiel wird ein ED25519-SSH-Schlüssel verwendet.

Folgende Kommandozeile prüft, ob auf dem Computer (VON welchem man AUF den MoneroPi zugreifen möchte) bereits ein SSH-Schlüssel vorhanden ist. Falls kein SSH-Schlüssel vorhanden ist wird selbiger erzeugt:

    if [[ ! -f ~/.ssh/id_ed25519.pub ]]; then ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519; fi

Anschließend kann der SSH-Schlüssel auf die MicroSD-Karte übertragen werden:

    mkdir /media/$USER/rootfs/home/pi/.ssh/
    cat ~/.ssh/id_ed25519*pub > /media/$USER/rootfs/home/pi/.ssh/authorized_keys

### optional: Hostnamen ändern

Standardmäßig lautet der Hostname (Computername) bei Raspbian \"raspberry\". In unserem Fall bietet sich die Umbenennung in \"MoneroPi\" an:

    sudo --shell
    cd /media/*/rootfs/etc
    sed --in-place 's/raspberrypi/MoneroPi/g' hostname hosts
    exit

Inbetriebnahme
--------------

Die MicroSD-Karte ist nun eingerichtet und der MoneroPi kann in Betrieb genommen werden:

1.  MicroSD-Karte am Computer auswerfen
2.  MicroSD-Karte in den Raspberry Pi einsetzen
3.  SSD mit Raspberry verbinden
4.  Raspberry Pi mit Netzwerk verbinden
5.  Raspberry Pi starten
6.  ggf. per SSH verbinden
7.  `sudo raspi-config`
    1.  Passwort ändern
    2.  WiFi einrichten
    3.  Lokalisierung anpassen
8.  `sudo reboot`

### automatische Systemaktualisierungen einrichten

Um die Paketquellen des MoneroPi täglich zu aktualisieren, mögliche Aktualisierung zu installieren und nicht benötigte Pakete zu entfernen, empfiehlt sich ein entsprechender Cronjob:

    sudo --shell
    cat <<EOT > /etc/cron.daily/distupgrade
    #! /usr/bin/env bash
    apt-get update
    apt-get --yes dist-upgrade
    apt-get --purge --yes autoremove
    EOT
    chmod +x /etc/cron.daily/distupgrade
    /etc/cron.daily/distupgrade # Funktionstest
    exit

### SSH-Server: Die Anmeldung per Passwort deaktivieren

Um die Anmeldung am SSH-Server per Passwort zu deaktivieren (nur noch Anmeldung via SSH-Schlüssel erlauben), sind folgende Kommadozeilen erforderlich:

    sudo --shell
    cp /etc/ssh/sshd_config /etc/ssh/sshd_config_$(date +%F_%T).bak
    sed --in-place '/#PasswordAuthentication yes/s/^#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
    service ssh restart
    exit

### Firewall einrichten

Um den SSH-Zugang auf Zugriffe aus dem lokalen Netz (Heimnetz) zu beschränken, kann die Firewall ufw verwendet werden.

**Im folgenden Beispiel werden nur Zugriffe aus den lokalen IP-Bereichen `IPv4 = 192.168.1.0/24` und `IPv6 = fe80::` zugelassen. Diese IP-Bereiche müssen den realen IP-Bereichen in eurem lokalen Netzwerk angepasst werden!**

    sudo --shell
    apt install --yes ufw
    ufw default allow outgoing
    ufw default reject incoming
    ufw allow from 192.168.1.0/24 to any app SSH # ggf. den IPv4-Bereich anpassen
    ufw allow from fe80:: to any app SSH # ggf. den IPv6-Bereich anpassen
    ufw enable
    exit

### SSD einrichten

Mit folgenden Kommandozeilen wird die SSD zunächst formatiert (GPT), dann partitioniert und für automatisches einhängen (mounten) bei Systemstart in \'/etc/fstab\' eingetragen:

    sudo --shell
    clear
    lsblk --nodeps --output NAME,VENDOR,MODEL,SIZE /dev/sd?
    echo
    read -p "Enter the NAME of the devices above you want to use for the monero-blockchain (e.g. 'sda'): " monero_blockchain_device
    monero_blockchain_device="/dev/"$monero_blockchain_device
    parted $monero_blockchain_device mklabel gpt
    parted --align optimal $monero_blockchain_device mkpart "Monero" 0% 100%
    mkdir /media/SSD
    cp /etc/fstab /etc/fstab_$(date +%F_%T).bak
    echo "${monero_blockchain_device}1 /media/SSD ext4 defaults 0 2" >> /etc/fstab
    reboot

### SWAP einrichten

Nach einer Standardinstallation von Raspbian Lite wird der Swapspeicher nach /var/swap geschrieben und die Größe auf 100 \[MB?\] begrenzt[^1]. Um die SD-Karte zu entlasten sollte der Swapsspeicher auf die SSD verlagert werden und der erforderliche Speicher sollte automatisch berechnet werden:

    sudo --shell
    touch /media/SSD/swap
    cp /etc/dphys-swapfile /etc/dphys-swapfile_$(date +%F_%T).bak
    sed --in-place "s/#CONF_SWAPFILE\=\/var\/swap/CONF_SWAPFILE\=\/media\/monero-blockchain\/swap/g" /etc/dphys-swapfile
    sed --in-place "s/CONF_SWAPSIZE\=100/CONF_SWAPSIZE\=/g" /etc/dphys-swapfile
    systemctl restart dphys-swapfile # der Neustart dieses Dienstes kann mehrere Minuten dauern ohne das sichtbar etwas passiert. Hier bitte geduldig bleiben! ;-)
    exit

Monero CLI
----------

### Installation

    wget --quiet --output-document /tmp/monero-linux-armv7.tar.bz2 https://downloads.getmonero.org/cli/linuxarm7
    wget --quiet --output-document /tmp/monero-hashes.txt https://getmonero.org/downloads/hashes.txt
    wget --quiet --output-document /tmp/binaryfate.asc https://raw.githubusercontent.com/monero-project/monero/master/utils/gpg_keys/binaryfate.asc
    clear
    gpg --keyid-format long --with-fingerprint /tmp/binaryfate.asc
    echo
    echo "Please verify the signing key above with the one on this site: https://web.getmonero.org/resources/user-guides/verification-allos-advanced.html#22-verify-signing-key"
    echo
    read -p "Hit enter to continue (if fingerprints do match) or cancel with Ctrl+C (if fingerprints do not match): "
    gpg --import /tmp/binaryfate.asc
    clear
    echo "Verifying the hash file:"
    gpg --verify /tmp/monero-hashes.txt
    echo
    read -p "Hit enter if you see 'Good signature from "binaryFate <binaryfate@getmonero.org>"' or cancel with Ctrl+C: "
    clear
    echo "Please verify the calculated fingerprint of /tmp/monero-linux-armv7.tar.bz2..."
    sha256sum /tmp/monero-linux-armv7.tar.bz2
    echo
    echo "with the signed fingerprint of /tmp/monero-hashes.txt:"
    cat /tmp/monero-hashes.txt | grep monero-linux-armv7
    echo
    read -p "Hit enter to continue (if fingerprints do match) or cancel with Ctrl+C (if fingerprints do not match): "
    sudo --shell
    tar --extract --bzip2 --file /tmp/monero-linux-armv7.tar.bz2 --directory /opt/
    for file in /opt/monero*/monero*; do ln -s $file /usr/local/bin/$(basename $file); done
    exit
    rm /tmp/monero-linux-armv7.tar.bz2 /tmp/monero-hashes.txt /tmp/binaryfate.asc

Um das Arbeitsverzeichnis auf der externen SSD zu verwenden, muss es zunächst eingerichtet und für Monero konfiguriert werden:

    sudo --shell
    mkdir /media/SSD/Monero
    chown --recursive pi:pi /media/SSD/Monero
    exit
    mkdir $HOME/.bitmonero/
    echo "data-dir=/media/SSD/Monero" >> $HOME/.bitmonero/bitmonero.conf

### Steuerung des Daemon

**HINWEIS**: Nachfolgend werden nur die gebräuchlichsten Funktionen des Monero Daemon beschrieben. Eine ausführliche Dokumentation der Optionen und Befehle erhält man unter: <https://monerodocs.org/interacting/monerod-reference/> (oder lokal mittels `monerod help`)

Um den Monero Daemon im Hintergrund laufen zu lassen, verwendet man folgendes Kommando:

    monerod --detach

Um die Erst-Synchronisation zu beschleunigen, kann den alten Blöcken auch \"blind\" vertraut werden:

    monerod --detach --fast-block-sync=1

Das war\'s! Der Monero Daemon läuft jetzt im Hintergrund und synchronisiert die Blockchain. Der aktuelle Status kann mit folgendem Befehl abgerufen werden:

    monerod status

Um nicht bei jedem Aufruf des Daemons ein neues SSL-Zertifikat zu generieren, kann die Option `--rpc-ssl=disabled` hinzugefügt werden:

    monerod --rpc-ssl=disabled status

Für regelmäßige Statusmeldungen kann eine Schleife mit Wartezeit sinnvoll sein:

    while true; do monerod --rpc-ssl=disabled status; sleep 600; clear; done
(im diesem Beispiel: 10 Minuten = 10 x 60 Sekunden = 600 Sekunden)

Um den Daemon zu stoppen, kann folgendes Kommando verwendet werden:

    monerod --rpc-ssl=disabled exit

Um den Monero Daemon nach jedem Neustart automatisch zu starten, empfiehlt sich die Einrichtung eines entsprechenden Cronjobs:

    sudo --shell
    echo "@reboot pi monerod --detach" >> /etc/crontab
    exit

### Aktualisierungen

Wenn neue Versionen der Monero-CLI für ARM veröffentlicht wurden, kann das System mit folgenden Kommandozeilen aktualisiert werden:


    wget --quiet --output-document /tmp/monero-linux-armv7.tar.bz2 https://downloads.getmonero.org/cli/linuxarm7
    wget --quiet --output-document /tmp/monero-hashes.txt https://getmonero.org/downloads/hashes.txt
    clear
    echo "Verifying the hash file:"
    gpg --verify /tmp/monero-hashes.txt
    echo
    read -p "Hit enter if you see 'Good signature from "binaryFate <binaryfate@getmonero.org>"' or cancel with Ctrl+C: "
    clear
    echo "Please verify the calculated fingerprint of /tmp/monero-linux-armv7.tar.bz2..."
    sha256sum /tmp/monero-linux-armv7.tar.bz2
    echo
    echo "...with the signed fingerprint of /tmp/monero-hashes.txt:"
    cat /tmp/monero-hashes.txt | grep monero-linux-armv7
    echo
    read -p "Hit enter to continue (if fingerprints do match) or cancel with Ctrl+C (if fingerprints do not match): "
    monerod exit
    sudo --shell
    rm /usr/local/bin/monero*
    tar --extract --bzip2 --file /tmp/monero-linux-armv7.tar.bz2 --directory /opt/
    rm /tmp/monero-linux-armv7.tar.bz2 /tmp/monero-hashes.txt
    for file in /opt/monero*/monero*; do ln -s $file /usr/local/bin/$(basename $file); done
    exit
    monerod --detach

------------------------------------------------------------------------

Wenn dir diese Dokumentation gefällt, oder sie hilfreich für dich war, dann würde ich mich über eine Spende freuen!

|                                          Monero Spende                                            |
| :-----------------------------------------------------------------------------------------------: |
| ![](https://gitlab.com/TalusUnheil/moneropi/-/raw/master/media/MoneroPi-XMR-Donation-QR-Code.png) |
|  86KZCj342BGBsdeTR6thhiagLJqrM4mhd1PEoj5e8koiNdXW4yYnFnmE73sDstAZjcWmgAyK37gWkNHgoJ7fivUqShwStrS  |

[^1]: Stand: Mai 2020
