[![](https://gitlab.com/TalusUnheil/moneropi/-/raw/transform_to_markdown/media/MoneroPi_Logo_height_200_px.png)](https://gitlab.com/TalusUnheil/moneropi/-/raw/transform_to_markdown/media/MoneroPi_Logo_height_200_px.png)

MoneroPi
========

The aim of the MoneroPi project is to operate a Monero CLI Full Node on a (headless) Raspberry Pi.

Preface
-------

This documentation was created as part of the German-language podcast \"MoneroMumble\" ([MoneroMumble.de](https://moneromumble.de/)). This documentation was **not** tested (as of May 2020), but was created in parallel to a first attempt. **This documentation is therefore still in an experimental phase and should not be used in connection with real
values (XMR)!**

The latest version of the documentation is [available on GitLab](https://gitlab.com/TalusUnheil/moneropi) and licensed under [GNU FDLv1.3](https://www.gnu.org/licenses/fdl-1.3.html).
If you like this documentation, or if it was helpful for you, I would appreciate a donation!

|                                         Monero donation                                           |
| :-----------------------------------------------------------------------------------------------: |
| ![](https://gitlab.com/TalusUnheil/moneropi/-/raw/master/media/MoneroPi-XMR-Donation-QR-Code.png) |
|  86KZCj342BGBsdeTR6thhiagLJqrM4mhd1PEoj5e8koiNdXW4yYnFnmE73sDstAZjcWmgAyK37gWkNHgoJ7fivUqShwStrS  |

Hardware
--------

-   Raspberry Pi 3 Model B
-   Power supply
-   Network cable
-   min. 4 GB MicroSD-Card (in a test `df --human` delivered less than 3 GB of used memory)
-   USB to SATA adapter cable
-   2,5\" SSD (on the page <https://moneroblocks.info/stats/blockchain-growth> you will find the current memory requirements of the blockchain at the very bottom - when purchasing the SSD you should plan for sufficient reserve!)

Prepare MicroSD card
--------------------

This manual requires a Linux PC. Specifically, this guide was created on a PC running Linux Mint 19 Cinnamon.

1.  Download [Raspbian Lite](https://downloads.raspberrypi.org/raspbian_lite_latest.torrent)
2.  Calculate the checksum of the download and compare it with the checksum on the website (CLI: `sha256sum ~/Downloads/*raspbian-buster-lite.zip`)
3.  Unpack the archive (CLI: `unzip ~/Downloads/*raspbian-buster-lite.zip ~/Downloads/`)
4.  Connect the MicroSD card to the PC and start the \"Disks\" FIXME program (GUI: Start menu -\> Accessories -\> Disks; CLI: `gnome-disks`)
    1.  Format the drive
    2.  Create a new FAT partition
    3.  Restore drive image -\> \~/Downloads/\*raspbian-buster-lite.img
5.  Delete the download and the disk image (CLI: `rm ~/Downloads/*raspbian-buster-lite.zip ~/Downloads/*raspbian-buster-lite.img`)

### optional: activate SSH server

If you do not want to operate the MoneroPi with screen and keyboard (\'headless\'), then control via SSH is recommended. However, the SSH server is not activated by default in Raspbian, so manual activation is required.

1.  Open the file manager
2.  under \"Devices\"FIXME open the partition \"boot\" of the MicroSD card
3.  create an empty file named \"ssh\" without file extension (CLI: `touch /media/$USER/boot/ssh`)

### optional: transfer SSH key

For remote control of the MoneroPi via SSH, an SSH key should be used instead of a password to log in. In this example an ED25519 SSH key is used.

The following command line checks whether an SSH key is already present on the computer from which you want to access the MoneroPi. If no SSH key is available, it will be generated:

    if [[ ! -f ~/.ssh/id_ed25519.pub ]]; then ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519; fi

The SSH key can then be transferred to the MicroSD card:

    mkdir /media/$USER/rootfs/home/pi/.ssh/
    cat ~/.ssh/id_ed25519*pub > /media/$USER/rootfs/home/pi/.ssh/authorized_keys

### optional: change hostname

By default, the host name (computer name) for Raspbian is \"raspberry\". In our case the renaming to \"MoneroPi\" is a good idea:

    sudo --shell
    cd /media/*/rootfs/etc
    sed --in-place 's/raspberrypi/MoneroPi/g' hostname hosts
    exit

Initial operation
-----------------

The MicroSD card is now set up and the MoneroPi can be put into operation:

1.  Eject the MicroSD card from the computer
2.  Insert the MicroSD card into the Raspberry Pi
3.  Connect the SSD with the Raspberry Pi
4.  Connect the Raspberry Pi to network
5.  Start the Raspberry Pi
6.  Connect via SSH from remote if necessary
7.  `sudo raspi-config`
    1.  Change password
    2.  WiFi setup
    3.  Customize localization
8.  `sudo reboot`

### Set up automatic system updates

To update the package sources of the MoneroPi on a daily basis, to install possible updates and to remove unneeded packages, a corresponding cronjob is recommended:

    sudo --shell
    cat <<EOT > /etc/cron.daily/distupgrade
    #! /usr/bin/env bash
    apt-get update
    apt-get --yes dist-upgrade
    apt-get --purge --yes autoremove
    EOT
    chmod +x /etc/cron.daily/distupgrade
    /etc/cron.daily/distupgrade # Functional test
    exit

### SSH server: Disable password login

To disable password login to the SSH server (only allow login via SSH key), the following command lines are required:

    sudo --shell
    cp /etc/ssh/sshd_config /etc/ssh/sshd_config_$(date +%F_%T).bak
    sed --in-place '/#PasswordAuthentication yes/s/^#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
    service ssh restart
    exit

### Set up firewall

To restrict SSH access to access from the local network (home network), the firewall ufw can be used.

**In the following example only accesses from the local IP ranges `IPv4 = 192.168.1.0/24` and `IPv6 = fe80::` are allowed. These IP ranges must be adapted to the real IP ranges in your local network!**

    sudo --shell
    apt install --yes ufw
    ufw default allow outgoing
    ufw default reject incoming
    ufw allow from 192.168.1.0/24 to any app SSH # ggf. den IPv4-Bereich anpassen
    ufw allow from fe80:: to any app SSH # ggf. den IPv6-Bereich anpassen
    ufw enable
    exit

### Setting up the SSD

With the following command lines the SSD is first formatted (GPT), then partitioned and entered in \'/etc/fstab\' for automatic mounting at system startup:

    sudo --shell
    clear
    lsblk --nodeps --output NAME,VENDOR,MODEL,SIZE /dev/sd?
    echo
    read -p "Enter the NAME of the devices above you want to use for the monero-blockchain (e.g. 'sda'): " monero_blockchain_device
    monero_blockchain_device="/dev/"$monero_blockchain_device
    parted $monero_blockchain_device mklabel gpt
    parted --align optimal $monero_blockchain_device mkpart "Monero" 0% 100%
    mkdir /media/SSD
    cp /etc/fstab /etc/fstab_$(date +%F_%T).bak
    echo "${monero_blockchain_device}1 /media/SSD ext4 defaults 0 2" >> /etc/fstab
    reboot

### Setting up SWAP

After a standard installation of Raspbian Lite, the swap space is written to /var/swap and the size is limited to 100 \[MB?\][^1]. To reduce the load on the SD card, the swap memory should be moved to the SSD and the required memory should be calculated automatically:

    sudo --shell
    touch /media/SSD/swap
    cp /etc/dphys-swapfile /etc/dphys-swapfile_$(date +%F_%T).bak
    sed --in-place "s/#CONF_SWAPFILE\=\/var\/swap/CONF_SWAPFILE\=\/media\/monero-blockchain\/swap/g" /etc/dphys-swapfile
    sed --in-place "s/CONF_SWAPSIZE\=100/CONF_SWAPSIZE\=/g" /etc/dphys-swapfile
    systemctl restart dphys-swapfile # the restart of this service can take several minutes without anything happening visibly. Please be patient here! ;-)
    exit

Monero CLI
----------

### Installation

    wget --quiet --output-document /tmp/monero-linux-armv7.tar.bz2 https://downloads.getmonero.org/cli/linuxarm7
    wget --quiet --output-document /tmp/monero-hashes.txt https://getmonero.org/downloads/hashes.txt
    wget --quiet --output-document /tmp/binaryfate.asc https://raw.githubusercontent.com/monero-project/monero/master/utils/gpg_keys/binaryfate.asc
    clear
    gpg --keyid-format long --with-fingerprint /tmp/binaryfate.asc
    echo
    echo "Please verify the signing key above with the one on this site: https://web.getmonero.org/resources/user-guides/verification-allos-advanced.html#22-verify-signing-key"
    echo
    read -p "Hit enter to continue (if fingerprints do match) or cancel with Ctrl+C (if fingerprints do not match): "
    gpg --import /tmp/binaryfate.asc
    clear
    echo "Verifying the hash file:"
    gpg --verify /tmp/monero-hashes.txt
    echo
    read -p "Hit enter if you see 'Good signature from "binaryFate <binaryfate@getmonero.org>"' or cancel with Ctrl+C: "
    clear
    echo "Please verify the calculated fingerprint of /tmp/monero-linux-armv7.tar.bz2..."
    sha256sum /tmp/monero-linux-armv7.tar.bz2
    echo
    echo "with the signed fingerprint of /tmp/monero-hashes.txt:"
    cat /tmp/monero-hashes.txt | grep monero-linux-armv7
    echo
    read -p "Hit enter to continue (if fingerprints do match) or cancel with Ctrl+C (if fingerprints do not match): "
    sudo --shell
    tar --extract --bzip2 --file /tmp/monero-linux-armv7.tar.bz2 --directory /opt/
    for file in /opt/monero*/monero*; do ln -s $file /usr/local/bin/$(basename $file); done
    exit
    rm /tmp/monero-linux-armv7.tar.bz2 /tmp/monero-hashes.txt /tmp/binaryfate.asc

To use the working directory on the external SSD, it must first be set up and configured for Monero:

    sudo --shell
    mkdir /media/SSD/Monero
    chown --recursive pi:pi /media/SSD/Monero
    exit
    mkdir $HOME/.bitmonero/
    echo "data-dir=/media/SSD/Monero" >> $HOME/.bitmonero/bitmonero.conf

### Controlling the daemon

**NOTE: Only the most common functions of the Monero daemon are described below. A detailed documentation of the options and commands can be found at <https://monerodocs.org/interacting/monerod-reference/> (or locally using `monerod help`)**

To run the Monero daemon in the background, use the following command:

    monerod --detach

To speed up the initial synchronization, the old blocks can also be trusted \"blind\":

    monerod --detach --fast-block-sync=1

That\'s it! The Monero Daemon now runs in the background and synchronizes the block chain. The current status can be retrieved with the following command:

    monerod status

To avoid generating a new SSL certificate every time the daemon is called, the option `--rpc-ssl=disabled` can be added:

    monerod --rpc-ssl=disabled status

For regular status messages a loop with waiting time can be useful:

    while true; do monerod --rpc-ssl=disabled status; sleep 600; clear; done
(in this example: 10 minutes = 10 x 60 seconds = 600 seconds)

To stop the daemon, the following command can be used:

    monerod --rpc-ssl=disabled exit

To start the Monero daemon automatically after each restart, it is recommended to set up a corresponding cron job:

    sudo --shell
    echo "@reboot pi monerod --detach" >> /etc/crontab
    exit

### Updating

When new versions of the Monero CLI for ARMv7 are released, the system can be updated with the following command lines:

    wget --quiet --output-document /tmp/monero-linux-armv7.tar.bz2 https://downloads.getmonero.org/cli/linuxarm7
    wget --quiet --output-document /tmp/monero-hashes.txt https://getmonero.org/downloads/hashes.txt
    clear
    echo "Verifying the hash file:"
    gpg --verify /tmp/monero-hashes.txt
    echo
    read -p "Hit enter if you see 'Good signature from "binaryFate <binaryfate@getmonero.org>"' or cancel with Ctrl+C: "
    clear
    echo "Please verify the calculated fingerprint of /tmp/monero-linux-armv7.tar.bz2..."
    sha256sum /tmp/monero-linux-armv7.tar.bz2
    echo
    echo "...with the signed fingerprint of /tmp/monero-hashes.txt:"
    cat /tmp/monero-hashes.txt | grep monero-linux-armv7
    echo
    read -p "Hit enter to continue (if fingerprints do match) or cancel with Ctrl+C (if fingerprints do not match): "
    monerod exit
    sudo --shell
    rm /usr/local/bin/monero*
    tar --extract --bzip2 --file /tmp/monero-linux-armv7.tar.bz2 --directory /opt/
    rm /tmp/monero-linux-armv7.tar.bz2 /tmp/monero-hashes.txt
    for file in /opt/monero*/monero*; do ln -s $file /usr/local/bin/$(basename $file); done
    exit
    monderod --detach

------------------------------------------------------------------------

If you like this documentation, or if it was helpful for you, I would appreciate a donation!

|                                         Monero donation                                           |
| :-----------------------------------------------------------------------------------------------: |
| ![](https://gitlab.com/TalusUnheil/moneropi/-/raw/master/media/MoneroPi-XMR-Donation-QR-Code.png) |
|  86KZCj342BGBsdeTR6thhiagLJqrM4mhd1PEoj5e8koiNdXW4yYnFnmE73sDstAZjcWmgAyK37gWkNHgoJ7fivUqShwStrS  |

[^1]: as of May 2020
